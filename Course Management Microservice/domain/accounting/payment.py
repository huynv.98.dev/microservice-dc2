from flask_sqlalchemy import SQLAlchemy
from ...app_monolith import app
db = SQLAlchemy(app)


class Payment(db.Model):
    __tablename__ = "payment"

    id = db.Column(db.Integer, primary_key=True)
    invoice_id = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.Integer, nullable=True)
    amount = db.Column(db.Float(2), nullable=False)
    