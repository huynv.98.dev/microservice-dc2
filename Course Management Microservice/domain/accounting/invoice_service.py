from ....common.db.invoice import init_db
def fetchall_invoices(limit, page):
  query = "select * from invoice"
  if limit and page:
    offset = (page -1) * limit
    query += f" limit {limit} offset {offset}"
  invoice_management_db_con = init_db.get_course_management_db()
  cur = invoice_management_db_con.cursor()
  cur.execute(query)
  invoices = cur.fetchall()
  return invoices

def fetchone_invoice(invoice_id):
  query = f"select * from invoice where id = {invoice_id}"
  invoice_management_db_con = init_db.get_course_management_db()
  cur = invoice_management_db_con.cursor()
  cur.execute(query)
  invoice = cur.fetchall()
  return invoice

def fetchall_payments(limit, page):
  query = "select * from payment"
  if limit and page:
    offset = (page -1) * limit
    query += f" limit {limit} offset {offset}"
  invoice_management_db_con = init_db.get_course_management_db()
  cur = invoice_management_db_con.cursor()
  cur.execute(query)
  payments = cur.fetchall()
  return payments

def fetchone_payment(invoice_id):
  query = f"select * from payment where id = {invoice_id}"
  invoice_management_db_con = init_db.get_course_management_db()
  cur = invoice_management_db_con.cursor()
  cur.execute(query)
  payment = cur.fetchall()
  return payment

def create_bill():
    pass

def create_payment():
    pass

def send_notification():
    pass