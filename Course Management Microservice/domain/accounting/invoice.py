from flask_sqlalchemy import SQLAlchemy
from ...app_monolith import app
db = SQLAlchemy(app)


class Invoice(db.Model):
    __tablename__ = "invoice"

    id = db.Column(db.Integer, primary_key=True)
    invoice_no = db.Column(db.String(80), unique=True, nullable=False)
    user_id = db.Column(db.Integer, nullable=True)
    course_id = db.Column(db.Integer, nullable=True)
    billing_amount = db.Column(db.Float(2), nullable=False)
    