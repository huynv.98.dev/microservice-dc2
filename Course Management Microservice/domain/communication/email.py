from flask_sqlalchemy import SQLAlchemy
from ...app_monolith import app
db = SQLAlchemy(app)

class Email:
    __tablename__= "email"

    id = db.Column(db.Integer, primary_key=True)
    notification_id = db.Column(db.Integer, nullable=False)
    sender = db.Column(db.String(255), nullable=False)
    receiver = db.Column(db.String(255), nullable=False)

    