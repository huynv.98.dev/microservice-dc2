from flask_sqlalchemy import SQLAlchemy
from ...app_monolith import app
db = SQLAlchemy(app)

class SMS:
    __tablename__= "sms"

    id = db.Column(db.Integer, primary_key=True)
    notification_id = db.Column(db.Integer, nullable=False)
    sender = db.Column(db.String(255), nullable=False)
    receiver = db.Column(db.String(255), nullable=False)

    