from common.db.user_management import init_db

def fetch_students(limit=10, page=0):
    offset = (page -1) * limit
    query = f"select * from user where role = 'student' limit {limit} offset {offset}"
    
    #TODO: Should use polling
    user_management_db_con = init_db.get_user_management_db()
    cr = user_management_db_con.cursor()
    
    cr.execute(query)
    users = cr.fetchall()
    return users