from common.db.course_management import init_db

def fetch_courses(limit, page):
  query = "select * from course"
  if limit and page:
    offset = (page -1) * limit
    query += f" limit {limit} offset {offset}"
  course_management_db_con = init_db.get_course_management_db()
  cur = course_management_db_con.cursor()
  cur.execute(query)
  courses = cur.fetchall()
  course_management_db_con.close()
  return courses

def fetch_course_by_id(id):
  query = f"select * from course where id = {id}"
  course_management_db_con = init_db.get_course_management_db()
  cur = course_management_db_con.cursor()
  cur.execute(query)
  courses = cur.fetchall()
  course_management_db_con.close()
  return courses[0] if len(courses) > 0 else None

def fetch_intakes_by_course(course_id, limit, page):
  query = f"select * from intake where course_id = {course_id}"
  if limit and page:
    offset = (page -1) * limit
    query += f" limit {limit} offset {offset}"
  course_management_db_con = init_db.get_course_management_db()
  cur = course_management_db_con.cursor()
  cur.execute(query)
  intakes = cur.fetchall()
  course_management_db_con.close()
  return intakes

def fetch_intake_by_id(id):
  query = f"select * from intake where id = {id}"
  course_management_db_con = init_db.get_course_management_db()
  cur = course_management_db_con.cursor()
  cur.execute(query)
  intakes = cur.fetchall()
  course_management_db_con.close()
  return intakes[0] if len(intakes) > 0 else None
