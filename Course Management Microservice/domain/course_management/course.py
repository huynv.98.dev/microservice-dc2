from flask_sqlalchemy import SQLAlchemy
from ...app_monolith import app
db = SQLAlchemy(app)

# Course class to represent a course object
class Course:
    __tablename__ = "course"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    price = db.Column(db.Float(2), nullable=False)
    image = db.Column(db.String(255), nullable=True)

# Define Class Intake with attributes and methods
class Intake:
    __tablename__ = "intake"

    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(80), nullable=False)
    start_date = db.Column(db.Date, nullable=False)
    end_date = db.Column(db.Date, nullable=False)
    status = db.Column(db.String(10), nullable=False)


