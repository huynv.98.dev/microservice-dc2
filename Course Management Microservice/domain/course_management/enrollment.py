from flask_sqlalchemy import SQLAlchemy
from ...app_monolith import app
db = SQLAlchemy(app)

#define Class Enrollment with attributes and methods
class Enrollment:
    __tablename__ = "enrollment"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    intake_id = db.Column(db.Integer, nullable=False)
    status = db.Column(db.String(10), nullable=False)

