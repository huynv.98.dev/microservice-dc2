from flask import Flask, jsonify, request
# from common.db import db
from common.generic_return import generic_return_success, generic_return_error
from domain.course_management import course_service
from domain.user_management import user_service

app = Flask(__name__)
# app.config.from_object()

def get_db_connection():
    pass

@app.route('/ping')
def ping():
    return {"message": "pong"}

@app.route('/api/courses')
def getCourses():
    page = int(request.args.get('page') or 1)
    limit = int(request.args.get('limit') or 10)
    return jsonify(generic_return_success(course_service.fetch_courses(limit, page)))

@app.route('/api/courses/<int:id>')
def findCourseById(id: int):
    return jsonify(generic_return_success(course_service.fetch_course_by_id(id)))

@app.route('/api/courses/<int:course_id>/intakes')
def getCourseIntakes(course_id: int):
    page = int(request.args.get('page') or 1)
    limit = int(request.args.get('limit') or 10)
    return jsonify(generic_return_success(course_service.fetch_intakes_by_course(course_id, limit, page)))

@app.route('/api/intakes/<int:id>')
def findIntakeById(id: int):
    return jsonify(generic_return_success(course_service.fetch_intake_by_id(id)))

@app.route('/api/students')
def getStudents():
    return jsonify(generic_return_success(user_service.fetch_students()))
