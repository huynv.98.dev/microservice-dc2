import sqlite3
import datetime
from pathlib import Path

connection = sqlite3.connect('database.db')
current_path = str(Path(__file__).parent.absolute())
print(current_path)

f = open(current_path + '/schema.sql')
connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO course (name, description, price, image) VALUES (?, ?, ?, ?)",
            ('Graphic Design', 'Graphic Design, online, self-paced (Elevate)', 
             4500, 'https://images.credly.com/images/0cd24741-5ca3-40bb-bef9-708428602f64/Credly_Badges_GD_-_Elevate.png')
            )

cur.execute("INSERT INTO course (name, description, price, image) VALUES (?, ?, ?, ?)",
            ('Cyber Security for Analysts', 'Cyber Security for Analysts, 12 months, flexible, part-time (Transform)', 
             11000, 'https://images.credly.com/images/68b84510-716e-4663-bfc0-2c6c610f108e/image.png')
            )

cur.execute("INSERT INTO course (name, description, price, image) VALUES (?, ?, ?, ?)",
            ('Front-End Web Development', 'Front-End Web Development, online, 18 weeks, part-time (Transform)', 
             9000, 'https://images.credly.com/images/2eaccd91-3148-4611-a92a-c97b3d32c5ec/Credly_Badges_Front-End_Web.png')
            )

connection.commit()

# insert intakes
cur.execute("select id, name from course")
courses = cur.fetchall() 
print(courses)
for course in courses:
    convert = map(lambda x: x[0].upper(), course[1].split(' '))
    cur.execute("INSERT INTO intake (course_id, name, start_date, end_date, 'status') VALUES (?, ?, ?, ?, ?)",
            (course[0], ''.join(list(convert)) + '-001', 
             datetime.date.today(), datetime.date.today() + datetime.timedelta(days=10), 'active')
            )
connection.commit()

connection.close()

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def get_course_management_db():
    connection = sqlite3.connect('database.db')
    connection.row_factory = dict_factory
    return connection

