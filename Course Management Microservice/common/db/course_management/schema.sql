DROP TABLE IF EXISTS course;

CREATE TABLE course (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    description TEXT NULL,
    price FLOAT NOT NULL DEFAULT 0,
    image TEXT NULL
);

DROP TABLE IF EXISTS intake;
CREATE TABLE intake (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    course_id INTEGER NOT NULL,
    name TEXT NULL,
    start_date Date NOT NULL,
    end_date Date NOT NULL,
    status TEXT NULL
);

DROP TABLE IF EXISTS enrollment;
CREATE TABLE enrollment (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER NOT NULL,
    intake_id INTEGER NOT NULL,
    status TEXT NULL
);