import sqlite3

connection = sqlite3.connect('database.db')


with open('schema.sql') as f:
    connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO course (title, description, price) VALUES (?, ?, ?)",
            ('Dump ways to die', 'Things you can do if you do not know what to do.', 1000)
            )

cur.execute("INSERT INTO course (title, description, price) VALUES (?, ?, ?)",
            ('Microservices for dummies',
             'A microservice course for 3 IQ students.', 1000)
            )

connection.commit()
connection.close()
