import sqlite3
import datetime
from pathlib import Path

connection = sqlite3.connect('database.db')
current_path = str(Path(__file__).parent.absolute())

f = open(current_path + '/schema.sql')
connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO user (name, role) VALUES (?, ?)",
            ('Mailovemisa', 'student'))
cur.execute("INSERT INTO user (name, role) VALUES (?, ?)",
            ('Meomaimaimeo', 'student'))
cur.execute("INSERT INTO user (name, role) VALUES (?, ?)",
            ('Peos', 'teacher'))
cur.execute("INSERT INTO user (name, role) VALUES (?, ?)",
            ('PiuPiu', 'teacher'))
cur.execute("INSERT INTO user (name, role) VALUES (?, ?)",
            ('Meomeo', 'admin'))


connection.commit()
connection.close()

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def get_user_management_db():
    connection = sqlite3.connect('database.db')
    connection.row_factory = dict_factory
    return connection

