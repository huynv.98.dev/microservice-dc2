import sqlite3
import datetime
from pathlib import Path

connection = sqlite3.connect('database.db')
current_path = str(Path(__file__).parent.absolute())
print(current_path)

f = open(current_path + '/schema.sql')
connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO notification (name, description, price, image) VALUES (?, ?, ?, ?)",
            ('Graphic Design', 'Graphic Design, online, self-paced (Elevate)', 
             4500, 'https://images.credly.com/images/0cd24741-5ca3-40bb-bef9-708428602f64/Credly_Badges_GD_-_Elevate.png')
            )


connection.commit()
connection.close()

