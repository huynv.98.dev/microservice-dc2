import json 

def generic_return_success(data):
    return {
        'status': 'success',
        'data': data
    }

def generic_return_error(error):
    return {
        'status': 'fail',
        'error': error
    }
    